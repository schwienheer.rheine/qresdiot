//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "zmq.h"

// Publisher section
ZMQ::Publisher::Publisher() {}

ZMQ::Publisher::Publisher(int _inPort) { m_port = _inPort; }

ZMQ::Publisher::~Publisher() {}

const std::vector<std::string> ZMQ::Publisher::getConfigAttributes() {
  return std::vector<std::string>({});
}

const std::string ZMQ::Publisher::identify() {
  return std::string("ZMQ::Publisher");
}

void ZMQ::Publisher::init() { return; }

void ZMQ::Publisher::pause() { return; }

void ZMQ::Publisher::run() { return; }

void ZMQ::Publisher::AcceptData(std::string _inData) { return; }

void ZMQ::Publisher::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  // if (_in == RUNNING)
  // run();
  // else if (_in == READY)
  // pause();
  // return;
}

// Receiver section
ZMQ::Receiver::Receiver() {}

ZMQ::Receiver::Receiver(std::string _inEndpoint) {
  m_patterns.clear();
  m_endpoint = _inEndpoint;
  m_state = NONE;
  m_listen_thread = NULL;
  m_known_bundles.clear();
  m_bundle_watch.clear();
}

ZMQ::Receiver::~Receiver() {
  // m_listen_thread->join();
  delete (m_listen_thread);
}

const std::vector<std::string> ZMQ::Receiver::getConfigAttributes() {
  return std::vector<std::string>({});
}

const std::string ZMQ::Receiver::identify() {
  return std::string("ZMQ::Receiver");
}

void ZMQ::Receiver::init() {
  m_state = READY;
  return;
}

void ZMQ::Receiver::pause() {
  // logMessage("Pausing...", "DBG");
  if (m_state == RUNNING) {
    m_state = READY;
    // logMessage("Joining...", "DBG");
    m_listen_thread->join();
    // logMessage("Joined.", "DBG");
  } else {
    m_state = READY;
  }
  return;
}

void ZMQ::Receiver::run() {
  // logMessage("Running...", "DBG");
  m_state = RUNNING;
  listen();
  return;
}

void ZMQ::Receiver::AcceptPatterns(std::vector<std::string> _inPatterns) {
  m_patterns.insert(m_patterns.end(), _inPatterns.begin(), _inPatterns.end());
  return;
}

void ZMQ::Receiver::AcceptPatterns(std::string _inPattern) {
  logMessage("Added adress: " + _inPattern, "DBG");
  m_patterns.push_back(_inPattern);
  return;
}

void ZMQ::Receiver::listen() {
  m_listen_thread = new std::thread([this]() {
    zmqpp::context context;
    zmqpp::socket_type type = zmqpp::socket_type::sub;
    zmqpp::socket socket(context, type);
    socket.connect(m_endpoint);
    socket.subscribe("tx ");
    zmqpp::reactor reactor;
    zmqpp::poller &poller = reactor.get_poller();
    auto socket_listener = [&poller, &socket, this]() {
      zmqpp::message response;
      if (poller.has_input(socket)) {
        socket.receive(response);
        std::string content;
        response >> content;
        // logMessage("Bundle hash: " +
        // Helpers::Basics::breakStringApart(content, " ").at(8), "DBG");
        // logMessage("TX hash: " + Helpers::Basics::breakStringApart(content, "
        // ").at(1), "DBG"); logMessage("Complete TX: " + content, "DBG");
        std::string adress =
            Helpers::Basics::breakStringApart(content, " ").at(2);
        std::string bundle_hash =
            Helpers::Basics::breakStringApart(content, " ").at(8);
        unsigned int bundles_content =
            std::stoul(Helpers::Basics::breakStringApart(content, " ").at(7));
        if (std::find(m_patterns.begin(), m_patterns.end(), adress) !=
            m_patterns.end()) {
          if (std::find(m_known_bundles.begin(), m_known_bundles.end(),
                        bundle_hash) == m_known_bundles.end()) {
            m_bundle_watch.push_back(std::make_tuple(bundle_hash, 1));
            m_known_bundles.push_back(bundle_hash);
            if (m_known_bundles.size() > 20)
              m_known_bundles.erase(m_known_bundles.begin());
            // FoundPatternMatch.emit(bundle_hash);
            // logMessage("Detected interesting bundle: " + bundle_hash,
            // "INFO");
          } else {
            for (unsigned int i = 0; i < m_bundle_watch.size(); i++) {
              if (std::get<0>(m_bundle_watch.at(i)) == bundle_hash) {
                unsigned int old = std::get<1>(m_bundle_watch.at(i));
                std::get<1>(m_bundle_watch.at(i)) = old + 1;
                if (old == bundles_content) {
                  FoundPatternMatch.emit(bundle_hash);
                  logMessage("Detected interesting bundle: " + bundle_hash,
                             "INFO");
                  m_bundle_watch.erase(m_bundle_watch.begin() + i);
                }
              }
            }
          }
        }
      }
    };
    reactor.add(socket, socket_listener);
    while (reactor.poll() && m_state == RUNNING) {
    }
  });
  return;
}

void ZMQ::Receiver::AcceptRunModeOrder(int _in) {
  // logMessage("Received RunModeOrder: " + std::to_string(_in), "DBG");
  forwardRunModeOrder(_in);
  if (_in == RUNNING && m_state != RUNNING) {
    run();
  } else if (_in == READY) {
    pause();
  } else if (_in == NONE) {
    pause();
  }
  return;
}
