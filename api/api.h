//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#ifndef API_H
#define API_H

#include <boost/dll.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "common/defaults.h"
#include "mam/mam.h"

#include <algorithm>
#include <fstream>
#include <sstream>

class API : public QResDIOTObject {
public:
  // Methods
  API();
  ~API();
  API(std::string);
  std::string evaluate();
  // Members
  // Signals
  sigslot::signal1<std::tuple<std::string, std::vector<std::string>>>
      ForwardMessageSendRequest;
  sigslot::signal1<std::string> ForwardNewTrustedEndpointAdded;
  sigslot::signal1<std::string> ForwardNewTrustedChannelAdded;
  // Slots
private:
  // Methods
  virtual const std::string identify();
  std::string apiNotAvailable(boost::property_tree::ptree);
  // Members
  std::string m_received_data;
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

#endif // API_H
