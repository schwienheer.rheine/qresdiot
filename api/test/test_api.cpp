//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "api/api.h"
#include "log/log.h"
#include <ctime>
#include <signal.h>
#include <thread>

sigslot::signal3<std::string, std::string, std::string> LogMessage;

const std::string identify() { return std::string("Test_API_App"); }

void signal_handler(int s) {
  if (s == 2) {
    // exit(-1);
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  API *api = new API(API_TEST_GET_HOSTNAME_JSON);
  LogMessage.emit(identify(), "Starting...", "INFO");
  LogMessage.emit(identify(), "Test GetHostname command...", "INFO");
  std::string evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_GET_OWN_NTRU_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test GetNTRUPubKeys command...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API("Bad command JSON");
  LogMessage.emit(identify(), "Test malformed request...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_UNKNOWN_COMMAND_JSON);
  LogMessage.emit(identify(), "Test unknown command...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_GET_TRUSTED_NTRU_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test GetTrustedNTRUPubKeys...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_GET_TRUSTED_CHANNEL_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test GetTrustedChannelPubKeys...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_GET_TRUSTED_ENDPOINT_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test GetTrustedEndpointPubKeys...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_ADD_TRUSTED_CHANNEL_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test AddTrustedChannelPubKeys...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_ADD_TRUSTED_ENDPOINT_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test AddTrustedEndpointPubKeys...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_ADD_TRUSTED_NTRU_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test AddTrustedNTRUPubKeys...", "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  api = new API(API_TEST_GET_ACTIVE_CHANNEL_ENDPOINT_PUBLIC_KEYS_JSON);
  LogMessage.emit(identify(), "Test GetActiveChannelEndpointPubKeys...",
                  "INFO");
  evalres = api->evaluate();
  delete (api);
  LogMessage.emit(identify(), "Evaluation result: " + evalres, "INFO");
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
