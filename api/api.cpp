//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "api.h"
#include <iostream>
#include <sstream>

template <typename T>
std::vector<T> as_vector(boost::property_tree::ptree const &pt,
                         boost::property_tree::ptree::key_type const &key) {
  std::vector<T> r;
  for (auto &item : pt.get_child(key))
    r.push_back(item.second.get_value<T>());
  return r;
}

static const bool containsOnlyTrytes(std::string _toTest) {
  std::string alphabetString(IOTA_TRYTE_ALPHABET);
  std::vector<char> alphabetVector(alphabetString.begin(),
                                   alphabetString.end());
  for (unsigned int i = 0; i < _toTest.length(); i++) {
    if (std::find(alphabetVector.begin(), alphabetVector.end(),
                  _toTest.at(i)) == alphabetVector.end()) {
      return false;
    }
  }
  return true;
}

static const bool isValid(unsigned int _length, std::string _toTest) {
  if (!containsOnlyTrytes(_toTest))
    return false;
  if (_toTest.length() != _length)
    return false;
  return true;
}

boost::property_tree::ptree
getAnswerSkeleton(boost::property_tree::ptree _req) {
  boost::property_tree::ptree result;
  result.put(APP_NAME_DESCRIPTOR,
             boost::dll::program_location().filename().string());
  result.put(QRESDIOT_API_VERSION_DESCRIPTOR, QRESDIOT_API_VERSION);
  result.push_back(std::make_pair(API_REQUEST_DESCRIPTOR, _req));
  return result;
}

boost::property_tree::ptree getAnswerSkeletonForBadRequest() {
  boost::property_tree::ptree result;
  result.put(APP_NAME_DESCRIPTOR,
             boost::dll::program_location().filename().string());
  result.put(QRESDIOT_API_VERSION_DESCRIPTOR, QRESDIOT_API_VERSION);
  boost::property_tree::ptree answer;
  boost::property_tree::ptree status;
  status.put("", "Malformed request");
  answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
  result.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
  return result;
}

API::API() {}

API::~API() {}

API::API(std::string _data) { m_received_data = _data; }

std::string API::evaluate() {
  std::string result = WS_API_BREACH_ANSWER;
  std::string command_s;
  std::stringstream data_stream(m_received_data);
  boost::property_tree::ptree pt;
  try {
    read_json(data_stream, pt);
    boost::property_tree::ptree &command = pt.get_child(API_CMD_STR_IDENTIFIER);
    command_s = command.get_value<std::string>();
  } catch (const boost::property_tree::ptree_error &e) {
    boost::property_tree::ptree res = getAnswerSkeletonForBadRequest();
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  }
  if (command_s.compare(API_CMD_GET_HOSTNAME) == 0) {
    std::string hostname;
    std::ifstream ifs;
    ifs.open(TOR_HOSTNAME_FILE_PATH, std::ifstream::in);
    std::getline(ifs, hostname);
    ifs.close();
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    boost::property_tree::ptree answervalue;
    if (hostname.length() == 0) {
      status.put("", API_ANSWER_FAIL_DESCRIPTOR);
      answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    } else {
      status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
      answervalue.put("", hostname);
      answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
      answer.add_child(API_HOSTNAME_DESCRIPTOR, answervalue);
    }
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_GET_OWN_NTRU_PUBLIC_KEYS) == 0) {
    MAM *mam = new MAM();
    std::vector<std::string> keys = mam->getNTRUPubKeys();
    delete (mam);
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    boost::property_tree::ptree answervalue;
    if (keys.size() == 0) {
      status.put("", API_ANSWER_FAIL_DESCRIPTOR);
      answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    } else {
      status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
      answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
      for (unsigned int i = 0; i < keys.size(); i++) {
        boost::property_tree::ptree temp;
        temp.put("", keys.at(i));
        answervalue.push_back(std::make_pair("", temp));
      }
      answer.add_child(API_NTRU_PUBLIC_KEY_ARRAY_DESCRIPTOR, answervalue);
    }
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_GET_TRUSTED_NTRU_PUBLIC_KEYS) == 0) {
    MAM *mam = new MAM();
    std::vector<std::string> keys = mam->getReceiverNTRUPubKeys();
    delete (mam);
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    boost::property_tree::ptree answervalue;
    status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    for (unsigned int i = 0; i < keys.size(); i++) {
      boost::property_tree::ptree temp;
      temp.put("", keys.at(i));
      answervalue.push_back(std::make_pair("", temp));
    }
    answer.add_child(API_NTRU_PUBLIC_KEY_ARRAY_DESCRIPTOR, answervalue);
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_ADD_TRUSTED_NTRU_PUBLIC_KEYS) == 0) {
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    MAM *mam = new MAM();
    std::vector<std::string> existing_keys = mam->getReceiverNTRUPubKeys();
    delete (mam);
    std::vector<std::string> pubKeys =
        as_vector<std::string>(pt, API_NTRU_PUBLIC_KEY_ARRAY_DESCRIPTOR);
    std::vector<std::string> accepted_keys;
    std::vector<std::string> rejected_keys;
    for (unsigned int i = 0; i < pubKeys.size(); i++) {
      if (isValid(MAM_NTRU_PUBLIC_KEY_LENGTH, pubKeys.at(i)) &&
          std::find(existing_keys.begin(), existing_keys.end(),
                    pubKeys.at(i)) == existing_keys.end())
        accepted_keys.push_back(pubKeys.at(i));
      else
        rejected_keys.push_back(pubKeys.at(i));
    }
    if (rejected_keys.size() == 0) {
      mam = new MAM();
      for (unsigned int i = 0; i < accepted_keys.size(); i++) {
        mam->AcceptReceiverNTRUPublicKey(accepted_keys.at(i));
      }
      delete (mam);
      status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    } else {
      status.put("", API_ANSWER_FAIL_DESCRIPTOR);
    }
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_REMOVE_TRUSTED_NTRU_PUBLIC_KEYS) == 0) {
    return result;
  } else if (command_s.compare(API_CMD_GET_TRUSTED_CHANNEL_PUBLIC_KEYS) == 0) {
    MAM *mam = new MAM();
    std::vector<std::string> keys = mam->getTrustedChannelPublicKeys();
    delete (mam);
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    boost::property_tree::ptree answervalue;
    status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    for (unsigned int i = 0; i < keys.size(); i++) {
      boost::property_tree::ptree temp;
      temp.put("", keys.at(i));
      answervalue.push_back(std::make_pair("", temp));
    }
    answer.add_child(API_CHANNEL_PUBLIC_KEY_ARRAY_DESCRIPTOR, answervalue);
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_GET_TRUSTED_ENDPOINT_PUBLIC_KEYS) == 0) {
    MAM *mam = new MAM();
    std::vector<std::string> keys = mam->getTrustedEndpointPublicKeys();
    delete (mam);
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    boost::property_tree::ptree answervalue;
    status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    for (unsigned int i = 0; i < keys.size(); i++) {
      boost::property_tree::ptree temp;
      temp.put("", keys.at(i));
      answervalue.push_back(std::make_pair("", temp));
    }
    answer.add_child(API_ENDPOINT_PUBLIC_KEY_ARRAY_DESCRIPTOR, answervalue);
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_ADD_TRUSTED_CHANNEL_PUBLIC_KEYS) == 0) {
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    MAM *mam = new MAM();
    std::vector<std::string> existing_keys = mam->getTrustedChannelPublicKeys();
    delete (mam);
    std::vector<std::string> pubKeys =
        as_vector<std::string>(pt, API_CHANNEL_PUBLIC_KEY_ARRAY_DESCRIPTOR);
    std::vector<std::string> accepted_keys;
    std::vector<std::string> rejected_keys;
    for (unsigned int i = 0; i < pubKeys.size(); i++) {
      if (isValid(MAM_CHANNEL_PUBLIC_KEY_LENGTH, pubKeys.at(i)) &&
          std::find(existing_keys.begin(), existing_keys.end(),
                    pubKeys.at(i)) == existing_keys.end())
        accepted_keys.push_back(pubKeys.at(i));
      else
        rejected_keys.push_back(pubKeys.at(i));
    }
    if (rejected_keys.size() == 0) {
      mam = new MAM();
      for (unsigned int i = 0; i < accepted_keys.size(); i++) {
        mam->AcceptNewTrustedChannel(accepted_keys.at(i));
        ForwardNewTrustedChannelAdded.emit(accepted_keys.at(i));
      }
      delete (mam);
      status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    } else {
      status.put("", API_ANSWER_FAIL_DESCRIPTOR);
    }
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_REMOVE_TRUSTED_CHANNEL_PUBLIC_KEYS) ==
             0) {
    return apiNotAvailable(pt);
  } else if (command_s.compare(API_CMD_ADD_TRUSTED_ENDPOINT_PUBLIC_KEYS) == 0) {
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    MAM *mam = new MAM();
    std::vector<std::string> existing_keys =
        mam->getTrustedEndpointPublicKeys();
    delete (mam);
    std::vector<std::string> pubKeys =
        as_vector<std::string>(pt, API_ENDPOINT_PUBLIC_KEY_ARRAY_DESCRIPTOR);
    std::vector<std::string> accepted_keys;
    std::vector<std::string> rejected_keys;
    for (unsigned int i = 0; i < pubKeys.size(); i++) {
      if (isValid(MAM_ENDPOINT_PUBLIC_KEY_LENGTH, pubKeys.at(i)) &&
          std::find(existing_keys.begin(), existing_keys.end(),
                    pubKeys.at(i)) == existing_keys.end())
        accepted_keys.push_back(pubKeys.at(i));
      else
        rejected_keys.push_back(pubKeys.at(i));
    }
    if (rejected_keys.size() == 0) {
      mam = new MAM();
      for (unsigned int i = 0; i < accepted_keys.size(); i++) {
        mam->AcceptNewTrustedEndpoint(accepted_keys.at(i));
        ForwardNewTrustedEndpointAdded.emit(accepted_keys.at(i));
      }
      delete (mam);
      status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    } else {
      status.put("", API_ANSWER_FAIL_DESCRIPTOR);
    }
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_REMOVE_TRUSTED_ENDPOINT_PUBLIC_KEYS) ==
             0) {
    return apiNotAvailable(pt);
  } else if (command_s.compare(
                 API_CMD_GET_ACTIVE_CHANNEL_ENDPOINT_PUBLIC_KEYS) == 0) {
    MAM *mam = new MAM();
    std::tuple<std::string, std::string> config =
        mam->getWorkingChannelConfig();
    delete (mam);
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    boost::property_tree::ptree channel;
    boost::property_tree::ptree endpoint;
    status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    channel.put("", std::get<0>(config));
    answer.push_back(
        std::make_pair(API_CHANNEL_PUBLIC_KEY_DESCRIPTOR, channel));
    endpoint.put("", std::get<1>(config));
    answer.push_back(
        std::make_pair(API_ENDPOINT_PUBLIC_KEY_DESCRIPTOR, endpoint));
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  } else if (command_s.compare(API_CMD_SEND_MESSAGE) == 0) {
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;

    boost::property_tree::ptree &mbody =
        pt.get_child(API_MESSAGE_BODY_DESCRIPTOR);
    std::string body = mbody.get_value<std::string>();
    if (body.empty()) {
      boost::property_tree::ptree tempmessage =
          pt.get_child(API_MESSAGE_BODY_DESCRIPTOR);
      std::ostringstream tempmessagestream;
      write_json(tempmessagestream, tempmessage);
      body = tempmessagestream.str();
    }
    std::vector<std::string> pubKeys =
        as_vector<std::string>(pt, API_NTRU_PUBLIC_KEY_ARRAY_DESCRIPTOR);
    std::vector<std::string> accepted_keys;
    std::vector<std::string> rejected_keys;
    for (unsigned int i = 0; i < pubKeys.size(); i++) {
      if (isValid(MAM_NTRU_PUBLIC_KEY_LENGTH, pubKeys.at(i)))
        accepted_keys.push_back(pubKeys.at(i));
      else
        rejected_keys.push_back(pubKeys.at(i));
    }
    status.put("", API_ANSWER_SUCCESS_DESCRIPTOR);
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    ForwardMessageSendRequest.emit(std::make_tuple(body, accepted_keys));
    return result;
  } else {
    boost::property_tree::ptree res = getAnswerSkeleton(pt);
    boost::property_tree::ptree answer;
    boost::property_tree::ptree status;
    status.put("", API_ANSWER_UNKNOWN_COMMAND);
    answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
    res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
    std::stringstream oss;
    write_json(oss, res);
    result = oss.str();
    return result;
  }
}

const std::string API::identify() { return std::string("API"); }

std::string API::apiNotAvailable(boost::property_tree::ptree _in) {
  std::string result;
  boost::property_tree::ptree res = getAnswerSkeleton(_in);
  boost::property_tree::ptree answer;
  boost::property_tree::ptree status;
  status.put("", API_ANSWER_COMMAND_NOT_AVAILABLE);
  answer.push_back(std::make_pair(API_STATUS_DESCRIPTOR, status));
  res.push_back(std::make_pair(API_ANSWER_DESCRIPTOR, answer));
  std::stringstream oss;
  write_json(oss, res);
  result = oss.str();
  return result;
}
