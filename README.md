# QResDIOT aka KUEdge #
- - - -
## Project purpose:
This projects aims to deliver a gateway for IOT (Internet of Things) devices to the [IOTA](https://www.iota.org/) tangle. It utilizes only Open-Source software and is designed to be as modular as possible.
A simple [webserver](https://gitlab.com/eidheim/Simple-Web-Server) is used to receive requests from any sensor service or data source. An API parses the requests and initiates the proper processing.Processing of a message to be sent basically consists of two steps:
1. Encrypt the message utilizing the [NTRU cryptosystem](https://en.wikipedia.org/wiki/NTRU) and prepare a [MAM](https://github.com/iotaledger/entangled/tree/develop/mam) bundle. This also means signing the message with the [Merkle tree signature scheme](https://en.wikipedia.org/wiki/Merkle_signature_scheme) that is widely used in [IOTA](https://www.iota.org/).
2. Send the bundle to the [IOTA](https://www.iota.org/) tangle using [CClient](https://github.com/iotaledger/entangled/tree/develop/cclient) for achieving semi-permanent storage.

The gateway also reads the stream from an [IRI](https://github.com/iotaledger/iri)-nodes ZMQ source. In this way, the device gets aware of messages from trusted *channels* and *endpoints*. The [bundle](https://docs.iota.org/docs/getting-started/0.1/transactions/bundles) hash is extracted from the ZMQ stream to be able to fetch the message [bundle](https://docs.iota.org/docs/getting-started/0.1/transactions/bundles). It then gets decrypted and the message can be processed further.

## How to use this project:
You have 2 options to use this project:
1. Run docker image from: https://hub.docker.com/r/kuhlmannmarkus/qresdiot
2. Build the project with bazel and run the `TestApp` binary. This is available only on Linux.

This project is designed for communication. Therefore it is pretty much useless without some kind of partner. I want to suggest to find someone, who is also willing to try out the capabilities of this project. The two of you would have to exchange your NTRU public keys (can be obtained via the API: `GetNTRUPubKeys`) and the public keys of your current channel and endpoint (obtained via the API: `GetActiveChannelEndpointPubKeys`). Every participant then has to introduce the counterpart's public keys to their own instance. This can be achieved via the API calls: `AddTrustedNTRUPubKeys`, `AddTrustedChannelPubKeys` and `AddTrustedEndpointPubKeys`. In this way you would be able to chat via the IOTA Tangle. Your messages are thought to be secure and NOT vulnerable to Shor's Algorithm and therefore safe against attacks with quantum computers. Read this [report](https://github.com/iotaledger/mam.c/blob/master/mam/rep.pdf) to learn more. Data immutability is NOT guaranteed by this project. You would need to trust the instances, you send messages to (since they are able to publish your contents where ever they want) and the instances, you receive messages from (the IOTA tangle delivers a mechanism of ordering transactions, which is probabilistic. Not every transactions is ordered, only the ones included in the tangle. This piece of software does NOT check the tangle history AT ALL).

## Dependencies:
* libboost-dev 
* libboost-program-options-dev 
* libboost-system-dev 
* libboost-filesystem-dev 
* libboost-iostreams-dev 
* libzmqpp-dev
* bazel

All other dependencies will be fetched by bazel during the build process. This includes:
* [sigslot - C++ Signal/Slot Library](http://sigslot.sourceforge.net/)
* [Entangled](https://github.com/iotaledger/entangled)
* [Simple-Web-Server](https://gitlab.com/eidheim/Simple-Web-Server)

This is the place to say thank you to everyone, who made my project possible:
* The Boost C++ team. [Boost C++](https://www.boost.org/)
* The ZeroMQ Team. [ZeroMQ](https://zeromq.org/)
* The Entangled team at the IOTA foundation. You guys are awesome! [Entangled](https://github.com/iotaledger/entangled)
* Sarah Thompson for the SigSlot library. [sigslot - C++ Signal/Slot Library](http://sigslot.sourceforge.net/)
* Ole Christian Eidheim for the Simple-Web-Server library. Not simple at all, but simple to use. [Simple-Web-Server](https://gitlab.com/eidheim/Simple-Web-Server)
* Rene Nyffenegger for the Base64 library. [cpp-base64](https://github.com/ReneNyffenegger/cpp-base64)
## Author:
Markus Kuhlmann
> I'm not a developer. Do NOT use this project in a productive environment.
> I'm working on this project on my weekends. I will not be able to improve anything in a sufficient timeframe and i will not be able to solve any issue, you might encounter.
## Project status:

- [x] API component
- [x] Log component
- [x] ZMQ listener component
- [ ] ZMQ publisher component
- [x] Webserver component
- [ ] Websocket server component
- [x] IOTA MAM API component
- [x] IOTA Tangle transport component
- [ ] CouchDB transport component
- [ ] Reactor component

## API commands: ##

**Get hostname:**
* Command: *GetHostname*
* Parameters: NONE
* Return fields: Hostname
* cURL example: `curl -X POST --data '{"Command":"GetHostname"}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"GetHostname"}').RawContent`

**Get own NTRU public keys:**
* Command: *GetNTRUPubKeys*
* Parameters: NONE
* Return fields: NTRUPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetNTRUPubKeys"}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"GetNTRUPubKeys"}').RawContent`
 
**Get trusted NTRU public keys:**
* Command: *GetTrustedNTRUPubKeys*
* Parameters: NONE
* Return fields: NTRUPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetTrustedNTRUPubKeys"}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"GetTrustedNTRUPubKeys"}').RawContent`

**Add trusted NTRU public keys:**
* Command: *AddTrustedNTRUPubKeys*
* Parameters: NTRUPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"AddTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"AddTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}').RawContent`

**Remove trusted NTRU public keys:**
* Command: ~~*RemTrustedNTRUPubKeys*~~
* Parameters: NTRUPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"RemTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"RemTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}').RawContent`

**Get trusted channel public keys:**
* Command: *GetTrustedChannelPubKeys*
* Parameters: NONE
* Return fields: ChannelPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetTrustedChannelPubKeys"}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"GetTrustedChannelPubKeys"}').RawContent`
 
**Add trusted channel public keys:**
* Command: *AddTrustedChannelPubKeys*
* Parameters: ChannelPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"AddTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"AddTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}').RawContent`
 
**Remove trusted channel public keys:**
* Command: ~~*RemTrustedChannelPubKeys*~~
* Parameters: ChannelPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"RemTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"RemTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}').RawContent`
 
**Get trusted endpoint public keys:**
* Command: *GetTrustedEndpointPubKeys*
* Parameters: NONE
* Return fields: EndpointPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetTrustedEndpointPubKeys"}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"GetTrustedEndpointPubKeys"}').RawContent`
 
**Add trusted endpoint public keys:**
* Command: *AddTrustedEndpointPubKeys*
* Parameters: EndpointPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"AddTrustedEndpointPubKeys","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"AddTrustedEndpointPubKeys","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}').RawContent`

**Remove trusted endpoint public keys:**
* Command: ~~*RemTrustedEndpointPubKeys*~~
* Parameters: EndpointPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"RemTrustedEndpointPubKeys","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"RemTrustedEndpointPubKeys","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}').RawContent`

**Get active channel and endpoint public keys:**
* Command: *GetActiveChannelEndpointPubKeys*
* Parameters: NONE
* Return fields: ChannelPubKey, EndpointPubKey
* cURL example: `curl -X POST --data '{"Command":"GetActiveChannelEndpointPubKeys"}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"GetActiveChannelEndpointPubKeys"}').RawContent`
 
**Send message:**
* Command: *SendMessage*
* Parameters: MessageBody, NTRUPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"SendMessage","MessageBody":"YOUR MESSAGE HERE","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}' localhost:1234/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:1234/json -Method Post -Body '{"Command":"SendMessage","MessageBody":"YOUR MESSAGE HERE","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}').RawContent`

## Bugs: ##

Yes. Maybe plenty! But I don't know where ;-)