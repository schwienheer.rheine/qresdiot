//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "web.h"

Web::WS::WS() {}

Web::WS::WS(int _in, std::string _mode, std::string _pubKey,
            std::string _privKey, std::string _caPubKey) {
  m_sec_server = NULL;
  m_server = NULL;
  m_mode = _mode; // TODO There is no mode anymore
  m_pubkey = _pubKey;
  m_privkey = _privKey;
  m_capubkey = _caPubKey;
  m_port = _in;
}

Web::WS::~WS() {
  logMessage("Killing Webserver...", "INFO");
  m_kill_thread = killServer();
  m_kill_thread->join();
  m_server_thread->join();
  delete (m_server_thread);
  delete (m_kill_thread);
  delete (m_server);
  delete (m_sec_server);
}

void Web::WS::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  if (_in == RUNNING)
    run();
  return;
}

void Web::WS::init() { return; }

void Web::WS::pause() { return; }

void Web::WS::run() {
  m_server_thread = startServer();
  return;
}

std::thread *Web::WS::startServer() {
  std::thread *server_thread;
  if (m_pubkey.length() != 0 && m_privkey.length() != 0) {
    m_sec_server = new SimpleWeb::Server<SimpleWeb::HTTPS>(m_pubkey, m_privkey,
                                                           m_capubkey);
    m_sec_server->config.port = m_port;
    m_sec_server->resource["^/json$"]["POST"] =
        [this](std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTPS>::Response>
                   response,
               std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTPS>::Request>
                   request) {
          try {
            boost::property_tree::ptree pt;
            read_json(request->content, pt);
            std::stringstream oss;
            write_json(oss, pt);
            API *api = new API(oss.str());
            std::string answer = api->evaluate();
            delete (api);
            *response << "HTTP/1.1 200 OK\r\n"
                      << "Content-Length: " << answer.length() << "\r\n\r\n"
                      << answer;
          } catch (const std::exception &e) {
            std::string answer = "API breach\n";
            *response << "HTTP/1.1 400 Bad Request\r\nContent-Length: "
                      << answer.length() << "\r\n\r\n"
                      << answer;
          }
        };
    server_thread = new std::thread([this]() { m_sec_server->start(); });
    std::this_thread::sleep_for(std::chrono::seconds(1));
  } else {
    logMessage("Spinning up Webserver on port: " + std::to_string(m_port),
               "DBG");
    m_server = new SimpleWeb::Server<SimpleWeb::HTTP>();
    m_server->config.port = m_port;

    m_server->resource["^/json$"]["POST"] =
        [this](std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTP>::Response>
                   response,
               std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTP>::Request>
                   request) {
          std::string answer;
          try {
            boost::property_tree::ptree pt;
            read_json(request->content, pt);
            std::stringstream oss;
            write_json(oss, pt);
            API *api = new API(oss.str());
            api->ForwardMessageSendRequest.connect(
                this, &Web::WS::AcceptMessageSendRequestForwarding);
            // api->ForwardNewTrustedEndpointAdded.connect(
            //    this, &Web::WS::AcceptNewTrustedEndpointAddedForwarding);
            api->ForwardNewTrustedChannelAdded.connect(
                this, &Web::WS::AcceptNewTrustedChannelAddedForwarding);
            answer = api->evaluate();
            api->ForwardMessageSendRequest.disconnect(this);
            // api->ForwardNewTrustedEndpointAdded.disconnect(this);
            api->ForwardNewTrustedChannelAdded.disconnect(this);
            delete (api);
            *response << "HTTP/1.1 200 OK\r\n"
                      << "Content-Length: " << answer.length() << "\r\n\r\n"
                      << answer;
          } catch (const std::exception &e) {
            answer = WS_API_BREACH_ANSWER;
            *response << "HTTP/1.1 400 Bad Request\r\nContent-Length: "
                      << answer.length() << "\r\n\r\n"
                      << answer;
          }
        };
    server_thread = new std::thread([this]() { m_server->start(); });
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  return server_thread;
}

std::thread *Web::WS::killServer() {
  std::thread *kill_thread = new std::thread([this]() {
    if (m_sec_server != NULL)
      m_sec_server->stop();
    if (m_server != NULL)
      m_server->stop();
  });
  return kill_thread;
}

const std::string Web::WS::identify() { return std::string("Web::WS"); }

void Web::WS::AcceptMessageSendRequestForwarding(
    std::tuple<std::string, std::vector<std::string>> _in) {
  MessageSendRequest.emit(_in);
  return;
}

void Web::WS::AcceptNewTrustedChannelAddedForwarding(std::string _in) {
  NewTrustedChannelAdded.emit(_in);
  return;
}

void Web::WS::AcceptNewTrustedEndpointAddedForwarding(std::string _in) {
  // NewTrustedEndpointAdded.emit(_in);
  return;
}

Web::WC::WC() {}

Web::WC::~WC() {}

void Web::WC::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  if (_in == RUNNING)
    run();
  return;
}

void Web::WC::init() { return; }

void Web::WC::pause() { return; }

void Web::WC::run() { return; }

const std::string Web::WC::identify() { return std::string("Web::WC"); }
