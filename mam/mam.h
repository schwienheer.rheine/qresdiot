//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef MAM_H
#define MAM_H

#include "common/trinary/trit_tryte.h"
#include "common/trinary/tryte.h"
#include "common/trinary/tryte_ascii.h"
#include "mam/api/api.h"
#include "mam/mam/mam_channel_t_set.h"
#include "mam/ntru/ntru.h"

#include "common/defaults.h"
#include "qresdiotobject/qresdiotobject.h"

#include "base64.h"

#include "gzip.h"

#include <algorithm>
#include <chrono>
#include <ctime>
#include <mutex>
#include <queue>
#include <random>
#include <stdio.h>
#include <sys/stat.h>
#include <thread>
#include <tuple>

class MAM : public QResDIOTObject {
public:
  // Methods
  MAM();
  ~MAM();
  std::vector<std::string> getNTRUPubKeys();
  std::vector<std::string> getReceiverNTRUPubKeys();
  std::vector<std::string> getTrustedChannelPublicKeys();
  std::vector<std::string> getTrustedEndpointPublicKeys();
  std::tuple<std::string, std::string> getWorkingChannelConfig();
  // Members
  // Signals
  sigslot::signal1<std::string> PayloadDecrypted;
  sigslot::signal1<bundle_transactions_t *> PayloadEncrypted;
  sigslot::signal1<std::string> NewTrustedEndpointAdded;
  sigslot::signal1<std::string> NewTrustedChannelAdded;
  sigslot::signal0<> MAMInitialized;
  // Slots
  virtual void AcceptRunModeOrder(int);
  void AcceptReceiverNTRUPublicKey(std::string);
  void AcceptData(std::tuple<std::string, std::vector<std::string>>);
  void AcceptNewTrustedChannel(std::string);
  void AcceptNewTrustedEndpoint(std::string);
  void AcceptBundleToDecrypt(bundle_transactions_t *);
  void AcceptConfigRequest();

private:
  // Methods
  virtual void init();
  virtual void pause();
  virtual void run();
  virtual const std::string identify();
  const bool fileexists(std::string);
  const std::string generateTrytes(unsigned int);
  void processCryptoQueue();
  void decryptBundle(bundle_transactions_t *);
  void decryptBundle(std::string);
  void encryptPayload(std::tuple<std::string, std::vector<std::string>>);
  std::vector<
      std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>>
  getChannelConfig();
  std::vector<std::string> getChannels();
  std::vector<std::string> getEndpoints();
  void channelMaintenance();
  void addReceiverNTRUPubKey(std::string);
  void addNewTrustedChannel(std::string);
  void addNewTrustedEndpoint(std::string);
  void removeReceiverNTRUPubKey(std::string);
  void removeNewTrustedChannel(std::string);
  void removeNewTrustedEndpoint(std::string);
  std::thread *startEncrypting();
  std::thread *startDecrypting();
  // Members
  std::mutex mtx;
  mam_api_t m_api;
  std::string m_path;
  std::string m_password;
  std::queue<std::tuple<std::string, std::vector<std::string>>>
      m_crypto_out_queue;
  std::queue<bundle_transactions_t *> m_crypto_in_queue;
  std::thread *m_encryption_thread;
  std::thread *m_decryption_thread;
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

#endif // MAM_H
