//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "mam.h"

MAM::MAM() {
  m_path = "/run/qresdiot/api.dat"; // TODO make api persistent in Docker
  m_password = "NOTAPASSWORD";      // TODO This is shitty! Change ASAP
  m_state = NONE;
  m_encryption_thread = NULL;
  m_decryption_thread = NULL;
}

MAM::~MAM() {
  delete (m_encryption_thread);
  delete (m_decryption_thread);
}

void MAM::init() {
  logMessage("Attempting to load API from file \"" + m_path + "\" ...", "DBG");
  mtx.lock();
  if (fileexists(m_path)) {
    mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
                 m_password.length());
    logMessage("Success!", "DBG");
  } else {
    logMessage("File \"" + m_path + "\" not found. Creating new API", "DBG");
    logMessage("Generating Seed and Nonce", "DBG");
    std::string _SEED = generateTrytes(81);
    mam_api_init(&m_api, (tryte_t *)_SEED.c_str());
    std::string _NONCE = generateTrytes(9);
    mam_ntru_sk_t ntru;
    MAM_TRITS_DEF(nonce, 3 * 9);
    nonce = MAM_TRITS_INIT(nonce, 3 * 9);
    trits_from_str(nonce, _NONCE.c_str());
    ntru_sk_reset(&ntru);
    logMessage("Generating NTRU keypair.", "DBG");
    ntru_sk_gen(&ntru, &m_api.prng, nonce);
    mam_api_add_ntru_sk(&m_api, &ntru);
    logMessage("Generating channel MT with depth " +
                   std::to_string(MAM_CH_EP_DEPTH_DEF),
               "DBG");
    tryte_t channel_id[MAM_CHANNEL_ID_TRYTE_SIZE];
    mam_api_channel_create(&m_api, MAM_CH_EP_DEPTH_DEF, channel_id);
    std::string channel_s = "";
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      channel_s += (char)channel_id[i];
    }
    logMessage("The channel ID is: " + channel_s, "DBG");
    logMessage("Generating endpoint MT with depth " +
                   std::to_string(MAM_CH_EP_DEPTH_DEF),
               "DBG");
    tryte_t endpoint_id[MAM_ENDPOINT_ID_TRYTE_SIZE];
    mam_api_endpoint_create(&m_api, MAM_CH_EP_DEPTH_DEF, channel_id,
                            endpoint_id);
    // trit_t message_id[MAM_MSG_ID_SIZE];
    // bundle_transactions_t *bundle = NULL;
    // bundle_transactions_new(&bundle);
    // mam_api_bundle_announce_endpoint(&m_api, channel_id, endpoint_id, NULL,
    //                                 NULL, bundle, message_id);
    // PayloadEncrypted.emit(bundle);
    // bundle_transactions_free(&bundle);
    std::string endpoint_s = "";
    for (unsigned int i = 0; i < MAM_ENDPOINT_ID_TRYTE_SIZE; i++) {
      endpoint_s += (char)endpoint_id[i];
    }
    logMessage("The endpoint ID is: " + endpoint_s, "DBG");
    logMessage("Saving API..", "DBG");
    mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
                 m_password.length());
  }
  mtx.unlock();
  mam_api_destroy(&m_api);
  MAMInitialized.emit();
  std::vector<std::string> channels = getTrustedChannelPublicKeys();
  for (unsigned int i = 0; i < channels.size(); i++) {
    NewTrustedChannelAdded.emit(channels.at(i));
  }
  std::vector<std::string> endpoints = getTrustedEndpointPublicKeys();
  for (unsigned int i = 0; i < endpoints.size(); i++) {
    NewTrustedEndpointAdded.emit(endpoints.at(i));
  }
  return;
}

void MAM::pause() {
  m_state = READY;
  m_encryption_thread->join();
  m_decryption_thread->join();
  return;
}

void MAM::run() {
  m_state = RUNNING;
  m_encryption_thread = startEncrypting();
  m_decryption_thread = startDecrypting();
  return;
}

void MAM::AcceptRunModeOrder(int _in) {
  if (_in == RUNNING && m_state == NONE) {
    m_state = RUNNING;
    init();
    run();
  } else if (_in == RUNNING && m_state == READY) {
    m_state = RUNNING;
    run();
  } else if (_in == READY && m_state == NONE) {
    init();
    m_state = READY;
  } else if (_in == READY && m_state == RUNNING) {
    pause();
    m_state = READY;
  } else if (_in == NONE && m_state == RUNNING) {
    m_state = NONE;
    pause();
  } else if (_in == NONE && m_state == READY) {
    m_state = NONE;
  }
  forwardRunModeOrder(_in);
  return;
}

const std::string MAM::identify() { return std::string("MAM"); }

const bool MAM::fileexists(std::string _name) {
  struct stat buffer;
  return (stat(_name.c_str(), &buffer) == 0);
}

const std::string MAM::generateTrytes(unsigned int _size) {
  const unsigned int randseed =
      std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 generator(randseed);
  std::string result = std::string();
  int random = 0;
  for (unsigned int i = 0; i < _size; i++) {
    random = generator() % 27;
    result += IOTA_TRYTE_ALPHABET[random];
  }
  return result;
}

void MAM::AcceptData(std::tuple<std::string, std::vector<std::string>> _in) {
  m_crypto_out_queue.push(_in);
  return;
}

void MAM::processCryptoQueue() {
  while (m_state == RUNNING) {
    break;
  }
  return;
}

void MAM::encryptPayload(
    std::tuple<std::string, std::vector<std::string>> _in) {
  retcode_t ret;
  std::string test123 = "iuwghiuerhgvbuer";

  logMessage("Encrypting payload...", "INFO");
  std::string raw_data = std::get<0>(_in);
  std::string compressed_data = Gzip::compress(raw_data);
  logMessage("Raw payload is: " + raw_data, "DBG");
  std::string data = base64_encode(
      reinterpret_cast<const unsigned char *>(compressed_data.c_str()),
      compressed_data.length());
  logMessage("Base64 encrypted and gzip compressed payload is: " + data, "DBG");
  std::vector<std::string> ntru_keys = std::get<1>(_in);
  size_t size = strlen(data.c_str()) * 2;
  tryte_t buffer[size];
  ascii_to_trytes(data.c_str(), buffer);
  bundle_transactions_t *bundle = NULL;
  bundle_transactions_new(&bundle);
  trit_t message_id[MAM_MSG_ID_SIZE];
  std::tuple<std::string, std::string> cfgnow = getWorkingChannelConfig();
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  logMessage("Writing header...", "DBG");
  clock_t begin = clock();
  if (ntru_keys.size() == 0) {
    ret = mam_api_bundle_write_header_on_endpoint(
        &m_api, (tryte_t *)std::get<0>(cfgnow).c_str(),
        (tryte_t *)std::get<1>(cfgnow).c_str(), NULL, m_api.ntru_pks, bundle,
        message_id);
    if (ret != RC_OK)
      logMessage("Error writing header", "ERR");
  } else {
    mam_api_t temp_api;
    mam_api_init(&temp_api, (tryte_t *)MAM_EMPTY_SEED);
    for (unsigned int i = 0; i < ntru_keys.size(); i++) {
      mam_ntru_pk_t new_pk;
      trit_t key_trit_t[MAM_NTRU_PK_SIZE];
      trytes_to_trits((tryte_t *)ntru_keys.at(i).c_str(), key_trit_t,
                      ntru_keys.at(i).length());
      for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE; i++) {
        new_pk.key[i] = key_trit_t[i];
      }
      ret = mam_api_add_ntru_pk(&temp_api, &new_pk);
      if (ret != RC_OK)
        logMessage("Error adding NTRU PK", "ERR");
    }
    ret = mam_api_bundle_write_header_on_endpoint(
        &m_api, (tryte_t *)std::get<0>(cfgnow).c_str(),
        (tryte_t *)std::get<1>(cfgnow).c_str(), NULL, temp_api.ntru_pks, bundle,
        message_id);
    if (ret != RC_OK)
      logMessage("Error writing header", "ERR");
    mam_api_destroy(&temp_api);
  }

  logMessage("...Done", "DBG");
  logMessage("Writing packet...", "DBG");
  ret = mam_api_bundle_write_packet(&m_api, message_id, buffer, size,
                                    MAM_MSG_CHECKSUM_SIG, true, bundle);
  if (ret != RC_OK)
    logMessage("Error writing packet", "ERR");
  else {
    clock_t end = clock();
    logMessage("...Done", "DBG");
    logMessage("Payload successfully encrypted in " +
                   std::to_string(double(end - begin) / CLOCKS_PER_SEC) + "s!",
               "INFO");
    ret = mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
                       m_password.length());
    if (ret != RC_OK)
      logMessage("Error saving API", "ERR");
    else {

      PayloadEncrypted.emit(bundle);
    }
  }
  mtx.unlock();
  // if(ret != RC_OK) logMessage("Error writing packet", "ERR");
  // logMessage("...Done", "DBG");
  mam_api_destroy(&m_api);
  // PayloadEncrypted.emit(bundle);
  return;
}

std::vector<std::string> MAM::getNTRUPubKeys() {
  logMessage("Fetching own NTRU public keys...", "DBG");
  std::vector<std::string> result;
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_ntru_sk_t_set_entry_t *entry;
  mam_ntru_sk_t_set_entry_t *tmp;
  SET_ITER(m_api.ntru_sks, entry, tmp) {
    tryte_t pk_temp[MAM_NTRU_PK_SIZE];
    trits_to_trytes(trits_begin(ntru_sk_pk_key(&entry->value)), pk_temp,
                    MAM_NTRU_PK_SIZE);
    std::string pkres = "";
    for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE / 3; i++) {
      pkres += (char)pk_temp[i];
    }
    result.push_back(pkres);
    logMessage("Own NTRU public key found: " + pkres, "DBG");
  }
  if (result.size() == 0)
    logMessage("No own NTRU public keys found", "DBG");
  mtx.unlock();
  mam_api_destroy(&m_api);
  return result;
}

std::vector<std::string> MAM::getReceiverNTRUPubKeys() {
  logMessage("Fetching receiver NTRU public keys...", "DBG");
  std::vector<std::string> result;
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_ntru_pk_t_set_entry_t *entry;
  mam_ntru_pk_t_set_entry_t *tmp;
  SET_ITER(m_api.ntru_pks, entry, tmp) {
    tryte_t pk_temp[MAM_NTRU_PK_SIZE];
    trits_to_trytes(trits_begin(mam_ntru_pk_key(&entry->value)), pk_temp,
                    MAM_NTRU_PK_SIZE);
    std::string pkres = "";
    for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE / 3; i++) {
      pkres += (char)pk_temp[i];
    }
    result.push_back(pkres);
    logMessage("NTRU receiver public key found: " + pkres, "DBG");
  }
  if (result.size() == 0)
    logMessage("No NTRU receiver public keys found", "DBG");
  mam_api_destroy(&m_api);
  mtx.unlock();
  return result;
}

void MAM::addReceiverNTRUPubKey(std::string _in) {
  logMessage("Adding receiver NTRU publix keys...", "DBG");
  if (_in.length() != MAM_NTRU_PK_SIZE / 3) {
    logMessage("Incorrect public key size!", "ERR");
    return;
  }
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_ntru_pk_t new_pk;
  trit_t key_trit_t[MAM_NTRU_PK_SIZE];
  trytes_to_trits((tryte_t *)_in.c_str(), key_trit_t, _in.length());
  for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE; i++) {
    new_pk.key[i] = key_trit_t[i];
  }
  mam_api_add_ntru_pk(&m_api, &new_pk);
  mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
               m_password.length());
  mtx.unlock();
  mam_api_destroy(&m_api);
  logMessage("Successfully added receiver public key: " + _in, "DBG");
  return;
}

void MAM::AcceptReceiverNTRUPublicKey(std::string _in) {
  addReceiverNTRUPubKey(_in);
  return;
}

void MAM::AcceptNewTrustedChannel(std::string _in) {
  addNewTrustedChannel(_in);
  return;
}

void MAM::AcceptNewTrustedEndpoint(std::string _in) {
  addNewTrustedEndpoint(_in);
  return;
}

void MAM::addNewTrustedChannel(std::string _in) {
  logMessage("Adding new trusted channel...", "DBG");
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_api_add_trusted_channel_pk(&m_api, (tryte_t *)_in.c_str());
  mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
               m_password.length());
  mtx.unlock();
  NewTrustedChannelAdded.emit(_in);
  mam_api_destroy(&m_api);
  return;
}

void MAM::addNewTrustedEndpoint(std::string _in) {
  retcode_t ret;
  logMessage("Adding new trusted endpoint...", "DBG");
  mtx.lock();
  ret = mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
                     m_password.length());
  if (ret != RC_OK)
    logMessage("Error loading API", "ERR");
  ret = mam_api_add_trusted_endpoint_pk(&m_api, (tryte_t *)_in.c_str());
  if (ret != RC_OK)
    logMessage("Error adding endpoint", "ERR");
  ret = mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
                     m_password.length());
  if (ret != RC_OK)
    logMessage("Error saving API", "ERR");
  mtx.unlock();
  // NewTrustedEndpointAdded.emit(_in);
  mam_api_destroy(&m_api);
  return;
}

std::vector<std::string> MAM::getTrustedChannelPublicKeys() {
  std::vector<std::string> own_keys = getChannels();
  std::vector<std::string> result;
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_pk_t_set_entry_t *entry;
  mam_pk_t_set_entry_t *tmp;
  SET_ITER(m_api.trusted_channel_pks, entry, tmp) {
    std::string pkres = "";
    char arr[MAM_CHANNEL_ID_TRIT_SIZE / 3];
    trits_to_str(trits_from_rep(MAM_CHANNEL_ID_TRIT_SIZE, entry->value.key),
                 arr);
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRIT_SIZE / 3; i++) {
      pkres += arr[i];
    }
    if (std::find(own_keys.begin(), own_keys.end(), pkres) == own_keys.end()) {
      result.push_back(pkres);
    }
  }
  if (result.size() == 0)
    logMessage("No trusted channel public keys found", "DBG");
  mtx.unlock();
  mam_api_destroy(&m_api);
  return result;
}

std::vector<std::string> MAM::getTrustedEndpointPublicKeys() {
  std::vector<std::string> own_keys = getEndpoints();
  std::vector<std::string> result;
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_pk_t_set_entry_t *entry;
  mam_pk_t_set_entry_t *tmp;
  SET_ITER(m_api.trusted_endpoint_pks, entry, tmp) {
    std::string pkres = "";
    char arr[MAM_CHANNEL_ID_TRIT_SIZE / 3];
    trits_to_str(trits_from_rep(MAM_CHANNEL_ID_TRIT_SIZE, entry->value.key),
                 arr);
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRIT_SIZE / 3; i++) {
      pkres += arr[i];
    }
    if (std::find(own_keys.begin(), own_keys.end(), pkres) == own_keys.end()) {
      result.push_back(pkres);
    }
  }
  if (result.size() == 0)
    logMessage("No trusted endpoint public keys found", "DBG");
  mtx.unlock();
  mam_api_destroy(&m_api);
  return result;
}

void MAM::AcceptBundleToDecrypt(bundle_transactions_t *_in) {
  m_crypto_in_queue.push(_in);
  return;
}

void MAM::decryptBundle(bundle_transactions_t *_in) {
  retcode_t ret;
  bool wasGood = false;
  logMessage("Decrypting bundle...", "INFO");
  std::vector<std::string> channels_old = getTrustedChannelPublicKeys();
  std::vector<std::string> endpoints_old = getTrustedEndpointPublicKeys();
  mtx.lock();
  ret = mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
                     m_password.length());
  if (ret != RC_OK)
    logMessage("Error loading API", "ERR");
  tryte_t *payload = NULL;
  size_t payload_size = 0;
  bool is_last_packet = false;
  clock_t begin = clock();
  ret = mam_api_bundle_read(&m_api, _in, &payload, &payload_size,
                            &is_last_packet);
  clock_t end = clock();
  if (ret != RC_OK)
    logMessage("Error reading bundle", "ERR");
  // if(is_last_packet) logMessage("Was last packet", "DBG");
  // else logMessage("Was not last packet", "ERR");
  if (payload_size != 0) {
    size_t size = payload_size / 2;
    char buffer[size];
    trytes_to_ascii(payload, strlen((char *)payload), buffer);
    std::string payload_s(buffer, size);
    logMessage("Payload in Base64 and gzip compressed is: " + payload_s, "DBG");
    std::string b64decoded_payload_s = base64_decode(payload_s);
    std::string uncompressed_payload = Gzip::decompress(b64decoded_payload_s);
    logMessage("Payload successfully decrypted in " +
                   std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
                   "s: " + uncompressed_payload,
               "INFO");
    PayloadDecrypted.emit(uncompressed_payload);
    free(payload);
    wasGood = true;
  }

  bundle_transactions_free(&_in);
  ret = mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
                     m_password.length());
  if (ret != RC_OK)
    logMessage("Error saving API", "ERR");
  mtx.unlock();
  mam_api_destroy(&m_api);
  std::vector<std::string> channels_new = getTrustedChannelPublicKeys();
  std::vector<std::string> endpoints_new = getTrustedEndpointPublicKeys();
  for (unsigned int i = 0; i < channels_new.size(); i++) {
    if (std::find(channels_old.begin(), channels_old.end(),
                  channels_new.at(i)) == channels_old.end()) {
      logMessage("Management bundle detected", "INFO");
      logMessage("Trusted channel added: " + channels_new.at(i), "INFO");
      wasGood = true;
      NewTrustedChannelAdded.emit(channels_new.at(i));
      // ret = mam_api_save(&m_api, m_path.c_str(), (tryte_t
      // *)m_password.c_str(),m_password.length()); if(ret != RC_OK)
      // logMessage("Error saving API", "ERR");
    }
  }
  for (unsigned int i = 0; i < endpoints_new.size(); i++) {
    if (std::find(endpoints_old.begin(), endpoints_old.end(),
                  endpoints_new.at(i)) == endpoints_old.end()) {
      logMessage("Management bundle detected", "INFO");
      logMessage("Trusted endpoint added: " + endpoints_new.at(i), "INFO");
      wasGood = true;
      // ret = mam_api_save(&m_api, m_path.c_str(), (tryte_t
      // *)m_password.c_str(),m_password.length()); if(ret != RC_OK)
      // logMessage("Error saving API", "ERR");
    }
  }
  if (!wasGood) {
    logMessage("This bundle was not meant for you or has no payload!", "INFO");
    if (!is_last_packet)
      logMessage("Was not last packet", "ERR");
  }
  return;
}

void MAM::decryptBundle(std::string _in) { return; }

void MAM::AcceptConfigRequest() { return; }

std::vector<
    std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>>
MAM::getChannelConfig() {
  std::vector<
      std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>>
      result;
  result.clear();
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_channel_t_set_entry_t *entry = NULL;
  mam_channel_t_set_entry_t *tmp = NULL;
  SET_ITER(m_api.channels, entry, tmp) {
    tryte_t channel_temp[MAM_CHANNEL_ID_TRYTE_SIZE];
    mam_endpoint_t_set_entry_t *epentry = NULL;
    mam_endpoint_t_set_entry_t *eptmp = NULL;
    trits_to_trytes(trits_begin(mam_channel_id(&entry->value)), channel_temp,
                    MAM_CHANNEL_ID_TRIT_SIZE);
    int remskschannel = mam_api_channel_remaining_sks(&m_api, channel_temp);
    std::string channel_s = "";
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      channel_s += (char)channel_temp[i];
    }
    logMessage("Found channel: " + channel_s + " with " +
                   std::to_string(remskschannel) + " remaining SKs",
               "DBG");
    std::vector<std::tuple<std::string, int>> endpoints_v;
    endpoints_v.clear();
    SET_ITER(entry->value.endpoints, epentry, eptmp) {
      tryte_t ep_temp[MAM_ENDPOINT_ID_TRYTE_SIZE];
      trits_to_trytes(trits_begin(mam_endpoint_id(&epentry->value)), ep_temp,
                      MAM_ENDPOINT_ID_TRIT_SIZE);
      int remsksendpoint =
          mam_api_endpoint_remaining_sks(&m_api, channel_temp, ep_temp);
      std::string ep_s = "";
      for (unsigned int i = 0; i < MAM_ENDPOINT_ID_TRYTE_SIZE; i++) {
        ep_s += (char)ep_temp[i];
      }
      endpoints_v.push_back(std::make_tuple(ep_s, remsksendpoint));
      logMessage("Found endpoint: " + ep_s + " with " +
                     std::to_string(remsksendpoint) + " remaining SKs",
                 "DBG");
    }
    result.push_back(std::make_tuple(channel_s, remskschannel, endpoints_v));
  }
  mtx.unlock();
  mam_api_destroy(&m_api);
  return result;
}

std::tuple<std::string, std::string> MAM::getWorkingChannelConfig() {
  logMessage("Fetching working channel and endpoint...", "DBG");
  channelMaintenance();
  std::vector<
      std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>>
      cfg = getChannelConfig();
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  tryte_t channel_id[81];
  tryte_t endpoint_id[81];
  for (unsigned int i = 0; i < 81; i++) {
    channel_id[i] = (tryte_t)std::get<0>(cfg.at(cfg.size() - 1))[i];
    endpoint_id[i] = (tryte_t)std::get<0>(
        std::get<2>(cfg.at(cfg.size() - 1))
            .at(std::get<2>(cfg.at(cfg.size() - 1)).size() - 1))[i];
  }
  std::string chan;
  std::string ep;
  for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
    chan += (char)channel_id[i];
    ep += (char)endpoint_id[i];
  }
  mtx.unlock();
  mam_api_destroy(&m_api);
  logMessage("Working config:\n\tChannel: " + chan + "\n\tEndpoint: " + ep,
             "DBG");
  return std::make_tuple(chan, ep);
}

void MAM::channelMaintenance() {
  std::vector<
      std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>>
      channelConfig = getChannelConfig();
  std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>
      latestChannel = channelConfig.at(channelConfig.size() - 1);
  std::tuple<std::string, int> latestEndpoint = std::make_tuple("INVALID", 0);
  if (std::get<2>(latestChannel).size() > 0)
    latestEndpoint =
        std::get<2>(latestChannel).at(std::get<2>(latestChannel).size() - 1);
  if (std::get<1>(latestEndpoint) >= 2)
    return;
  else {
    logMessage("Maintaining channel config...", "INFO");
    mtx.lock();
    mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
                 m_password.length());
    tryte_t channel_id[81];
    tryte_t endpoint_id[81];
    tryte_t new_channel_id[81];
    bool channelGenerated = false;
    for (unsigned int i = 0; i < 81; i++) {
      channel_id[i] = (tryte_t)std::get<0>(latestChannel)[i];
    }
    if (std::get<1>(latestChannel) < 3) {

      logMessage("Generating channel...", "INFO");
      mam_api_channel_create(&m_api, MAM_CH_EP_DEPTH_DEF, new_channel_id);
      trit_t channel_message_id[MAM_MSG_ID_SIZE];
      bundle_transactions_t *channelbundle = NULL;
      bundle_transactions_new(&channelbundle);
      mam_api_bundle_announce_channel(&m_api, channel_id, new_channel_id, NULL,
                                      NULL, channelbundle, channel_message_id);
      channelGenerated = true;
      std::string theChannel;
      for (unsigned int i = 0; i < 81; i++) {
        theChannel += new_channel_id[i];
      }
      logMessage("Channel generated: " + theChannel, "INFO");
      PayloadEncrypted.emit(channelbundle);
      // for (unsigned int i = 0; i < 81; i++) {
      //  channel_id[i] = new_channel_id[i];
      //}
    }
    logMessage("Generating endpoint...", "INFO");
    if (channelGenerated)
      mam_api_endpoint_create(&m_api, MAM_CH_EP_DEPTH_DEF, new_channel_id,
                              endpoint_id);
    else
      mam_api_endpoint_create(&m_api, MAM_CH_EP_DEPTH_DEF, channel_id,
                              endpoint_id);
    trit_t endpoint_message_id[MAM_MSG_ID_SIZE];
    bundle_transactions_t *endpointbundle = NULL;
    bundle_transactions_new(&endpointbundle);
    mam_api_bundle_announce_endpoint(&m_api, channel_id, endpoint_id, NULL,
                                     NULL, endpointbundle, endpoint_message_id);
    // std::this_thread::sleep_for(std::chrono::seconds(20));
    PayloadEncrypted.emit(endpointbundle);
    std::string theEndpoint;
    for (unsigned int i = 0; i < 81; i++) {
      theEndpoint += endpoint_id[i];
    }
    logMessage("Endpoint generated: " + theEndpoint, "INFO");
    mam_api_save(&m_api, m_path.c_str(), (tryte_t *)m_password.c_str(),
                 m_password.length());
    mtx.unlock();
    mam_api_destroy(&m_api);
  }
  getChannelConfig();
  return;
}

void MAM::removeReceiverNTRUPubKey(std::string _in) { return; }

void MAM::removeNewTrustedChannel(std::string _in) { return; }

void MAM::removeNewTrustedEndpoint(std::string _in) { return; }

std::vector<std::string> MAM::getChannels() {
  std::vector<std::string> result;
  result.clear();
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_channel_t_set_entry_t *entry = NULL;
  mam_channel_t_set_entry_t *tmp = NULL;
  SET_ITER(m_api.channels, entry, tmp) {
    tryte_t channel_temp[MAM_CHANNEL_ID_TRYTE_SIZE];
    trits_to_trytes(trits_begin(mam_channel_id(&entry->value)), channel_temp,
                    MAM_CHANNEL_ID_TRIT_SIZE);
    std::string channel_s = "";
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      channel_s += (char)channel_temp[i];
    }
    result.push_back(channel_s);
  }
  mtx.unlock();
  mam_api_destroy(&m_api);
  return result;
}

std::vector<std::string> MAM::getEndpoints() {
  std::vector<std::string> result;
  result.clear();
  mtx.lock();
  mam_api_load(m_path.c_str(), &m_api, (tryte_t *)m_password.c_str(),
               m_password.length());
  mam_channel_t_set_entry_t *entry = NULL;
  mam_channel_t_set_entry_t *tmp = NULL;
  SET_ITER(m_api.channels, entry, tmp) {
    tryte_t channel_temp[MAM_CHANNEL_ID_TRYTE_SIZE];
    mam_endpoint_t_set_entry_t *epentry = NULL;
    mam_endpoint_t_set_entry_t *eptmp = NULL;
    trits_to_trytes(trits_begin(mam_channel_id(&entry->value)), channel_temp,
                    MAM_CHANNEL_ID_TRIT_SIZE);
    std::string channel_s = "";
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      channel_s += (char)channel_temp[i];
    }
    SET_ITER(entry->value.endpoints, epentry, eptmp) {
      tryte_t ep_temp[MAM_ENDPOINT_ID_TRYTE_SIZE];
      trits_to_trytes(trits_begin(mam_endpoint_id(&epentry->value)), ep_temp,
                      MAM_ENDPOINT_ID_TRIT_SIZE);
      std::string ep_s = "";
      for (unsigned int i = 0; i < MAM_ENDPOINT_ID_TRYTE_SIZE; i++) {
        ep_s += (char)ep_temp[i];
      }
      result.push_back(ep_s);
    }
  }
  mtx.unlock();
  mam_api_destroy(&m_api);
  return result;
}

std::thread *MAM::startEncrypting() {
  std::thread *encryptor = new std::thread([this]() {
    while (m_state == RUNNING) {
      if (m_crypto_out_queue.size() == 0) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      }
      encryptPayload(m_crypto_out_queue.front());
      m_crypto_out_queue.pop();
    }
  });
  return encryptor;
}

std::thread *MAM::startDecrypting() {
  std::thread *decryptor = new std::thread([this]() {
    while (m_state == RUNNING) {
      if (m_crypto_in_queue.size() == 0) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      }
      decryptBundle(m_crypto_in_queue.front());
      m_crypto_in_queue.pop();
    }
  });
  return decryptor;
}
