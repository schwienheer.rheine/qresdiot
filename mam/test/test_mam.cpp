//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "log/log.h"
#include "mam/mam.h"
#include <ctime>
#include <signal.h>
#include <thread>

sigslot::signal3<std::string, std::string, std::string> LogMessage;
sigslot::signal1<int> RunModeOrder;

const std::string identify() { return std::string("Test_MAM_App"); }

void signal_handler(int s) {
  if (s == 2) {
    exit(-1);
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  MAM *mam = new MAM();
  mam->LogMessage.connect(log, &Log::AcceptLogMessage);
  RunModeOrder.connect(dynamic_cast<QResDIOTObject *>(mam),
                       &QResDIOTObject::AcceptRunModeOrder);
  LogMessage.emit(identify(), "Starting...", "INFO");
  RunModeOrder.emit(2);
  clock_t end = clock();
  RunModeOrder.disconnect(mam);
  mam->LogMessage.disconnect(log);
  delete (mam);
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
