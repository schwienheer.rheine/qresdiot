//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "qresdiotobject.h"

QResDIOTObject::QResDIOTObject() {
  m_state = NONE;
  m_timeofcreation = clock();
  m_children.clear();
}

QResDIOTObject::~QResDIOTObject() {}

const std::string QResDIOTObject::identify() {
  return std::string("QResDIOTObject");
}

const std::vector<std::string> QResDIOTObject::getConfigAttributes() {
  return std::vector<std::string>({});
}

void QResDIOTObject::logMessage(std::string _message, std::string _severity) {
  LogMessage.emit(identify(), _message, _severity);
  return;
}

void QResDIOTObject::AcceptLogMessagePassthrough(std::string _from,
                                                 std::string _message,
                                                 std::string _severity) {
  LogMessage.emit(_from, _message, _severity);
  return;
}

void QResDIOTObject::connectChild(QResDIOTObject *_child) {
  m_children.push_back(_child);
  _child->LogMessage.connect(dynamic_cast<QResDIOTObject *>(this),
                             &QResDIOTObject::AcceptLogMessagePassthrough);
  ForwardRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(_child),
                              &QResDIOTObject::AcceptRunModeOrder);
  return;
}

void QResDIOTObject::disconnectChild(QResDIOTObject *_child) {
  _child->LogMessage.disconnect(dynamic_cast<QResDIOTObject *>(this));
  ForwardRunModeOrder.disconnect(dynamic_cast<QResDIOTObject *>(_child));
  return;
}

void QResDIOTObject::init() { return; }

void QResDIOTObject::pause() { return; }

void QResDIOTObject::run() { return; }

void QResDIOTObject::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  return;
}

void QResDIOTObject::forwardRunModeOrder(int _in) {
  ForwardRunModeOrder.emit(_in);
  return;
}
