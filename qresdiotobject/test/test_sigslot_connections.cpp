//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "log/log.h"
#include "qresdiotobject/qresdiotobject.h"
#include <ctime>
#include <signal.h>

sigslot::signal3<std::string, std::string, std::string> LogMessage;
sigslot::signal1<int> RunModeOrder;

const std::string identify() {
  return std::string("Test_SigSlot_Connections_App");
}

class Derived : public QResDIOTObject {
public:
  Derived(){};
  ~Derived(){};
  void speak() { logMessage("Hello", "INFO"); };
  virtual void AcceptRunModeOrder(int _in) {
    forwardRunModeOrder(_in);
    logMessage("Received RMO: " + std::to_string(_in), "INFO");
    PublishRunMode.emit(_in);
  };
  void produceChild() {
    m_child = new Derived();
    connectChild(m_child);
    m_child->speak();
  };
  void killChild() {
    disconnectChild(m_child);
    delete (m_child);
  };

private:
  Derived *m_child;
  virtual const std::string identify() { return std::string("Derived"); };
};

void signal_handler(int s) {
  if (s == 2) {
    exit(-1);
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), "Starting...", "INFO");
  Derived *parent = new Derived();
  parent->LogMessage.connect(log, &Log::AcceptLogMessage);
  RunModeOrder.connect(dynamic_cast<QResDIOTObject *>(parent),
                       &QResDIOTObject::AcceptRunModeOrder);
  parent->produceChild();
  RunModeOrder.emit(1);
  parent->killChild();
  parent->LogMessage.disconnect(log);
  RunModeOrder.disconnect(parent);
  delete (parent);
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
