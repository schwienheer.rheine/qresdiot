//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "helpers.h"

std::vector<std::string>
Helpers::Basics::breakStringApart(std::string _inData,
                                  std::string _inSeparator) {
  std::vector<std::string> result;
  size_t pos = 0;
  std::string token = std::string("");
  // int count = 0;
  while ((pos = _inData.find(_inSeparator.c_str())) != std::string::npos) {
    token = _inData.substr(0, pos);
    _inData.erase(0, pos + _inSeparator.length());
    result.push_back(token);
    //  count++;
  }
  // result.clear();
  return result;
}
