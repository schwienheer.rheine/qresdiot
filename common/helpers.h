//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#ifndef HELPERS_H
#define HELPERS_H

#include <string>
#include <vector>

namespace Helpers {
namespace Trinary {}

namespace Basics {
std::vector<std::string> breakStringApart(std::string, std::string);
}

namespace Basics {}
}; // namespace Helpers

#endif // HELPERS_H
