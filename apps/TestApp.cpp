//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include <boost/program_options.hpp>
#include <iostream>
#include <signal.h>
#include <thread>

#include "log/log.h"
#include "mam/mam.h"
#include "tangle/tangle.h"
#include "web/web.h"
#include "zmq/zmq.h"

#include "common/defaults.h"
#include "common/helpers.h"

sigslot::signal3<std::string, std::string, std::string> LogMessage;
sigslot::signal1<int> SendRunModeOrder;
sigslot::signal1<std::vector<std::string>> SendZMQPatterns;

static volatile bool killswitch = false;

const std::string identify() { return std::string("TestApp"); }

void signal_handler(int s) {
  std::cout << "Caught Signal: " << s << std::endl;
  if (s == 2) {
    killswitch = true;
  }
}

int main(int argc, const char *argv[]) {
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  try {
    boost::program_options::options_description desc("Available options");
    desc.add_options()("help", "produce help message");
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }
  } catch (std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (...) {
    std::cerr << "Exception of unknown type!\n";
  }
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), "Starting...", "INFO");
  MAM *mam = new MAM();
  mam->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(mam),
                           &QResDIOTObject::AcceptRunModeOrder);
  Web::WS *ws = new Web::WS(API_PORT_DEF, "test");
  ws->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(ws),
                           &QResDIOTObject::AcceptRunModeOrder);
  ZMQ::Receiver *rec = new ZMQ::Receiver(ZMQ_REC_TANGLE_URI_DEF);
  rec->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(rec),
                           &QResDIOTObject::AcceptRunModeOrder);
  SendZMQPatterns.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  mam->NewTrustedChannelAdded.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  Tangle *tangle = new Tangle(
      TANGLE_NODE_HOST_DEF, TANGLE_NODE_PORT_DEF, TANGLE_NODE_REMOTE_POW_DEF,
      TANGLE_NODE_GTTA_DEPTH_DEF, TANGLE_NODE_POW_MWM_DEF, "");
  tangle->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(tangle),
                           &QResDIOTObject::AcceptRunModeOrder);
  ws->MessageSendRequest.connect(mam, &MAM::AcceptData);
  ws->NewTrustedChannelAdded.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  // ws->NewTrustedEndpointAdded.connect(
  //    rec, &ZMQ::Receiver::AcceptPatterns);
  mam->PayloadEncrypted.connect(tangle, &Tangle::AcceptSendRequest);
  rec->FoundPatternMatch.connect(tangle, &Tangle::AcceptFetchRequest);
  tangle->BundleFetched.connect(mam, &MAM::AcceptBundleToDecrypt);
  SendRunModeOrder.emit(2);
  LogMessage.emit(identify(), "Started", "INFO");
  while (!killswitch) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  LogMessage.emit(identify(), "Shutting down...", "INFO");
  SendRunModeOrder.emit(0);
  std::this_thread::sleep_for(std::chrono::seconds(1));
  LogMessage.emit(identify(), "Finished.", "INFO");
  ws->NewTrustedChannelAdded.disconnect(rec);
  // ws->NewTrustedEndpointAdded.disconnect(rec);
  tangle->BundleFetched.disconnect(mam);
  rec->FoundPatternMatch.disconnect(tangle);
  mam->PayloadEncrypted.disconnect(tangle);
  ws->MessageSendRequest.disconnect(mam);
  SendRunModeOrder.disconnect(rec);
  SendZMQPatterns.disconnect(rec);
  mam->NewTrustedChannelAdded.disconnect(rec);
  rec->LogMessage.disconnect(log);
  delete (rec);
  SendRunModeOrder.disconnect(ws);
  ws->LogMessage.disconnect(log);
  delete (ws);
  mam->LogMessage.disconnect(log);
  SendRunModeOrder.disconnect(mam);
  delete (mam);
  tangle->LogMessage.disconnect(log);
  SendRunModeOrder.disconnect(tangle);
  delete (tangle);
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
