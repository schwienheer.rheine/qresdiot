FROM ubuntu:18.04
RUN apt-get update
RUN apt install pkg-config zip g++ zlib1g-dev unzip python3 curl libboost-dev libboost-program-options-dev libboost-iostreams-dev libboost-system-dev libboost-filesystem-dev libsqlite3-dev git libssl-dev libzmqpp-dev -y && echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list && curl https://bazel.build/bazel-release.pub.gpg | apt-key add - && apt update && apt install bazel -y
RUN git clone https://gitlab.com/herrkpunkt/qresdiot.git && cd qresdiot && bazel build //...
RUN mkdir -p /run/qresdiot
VOLUME ["/run/qresdiot"]
COPY start.sh /start.sh
CMD ["./start.sh"]
EXPOSE 1234
