//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef TANGLE_H
#define TANGLE_H

#include "qresdiotobject/qresdiotobject.h"
#include <fstream>
#include <queue>
#include <streambuf>
#include <thread>
#include <algorithm>

#include "cclient/api/core/core_api.h"
#include "cclient/api/extended/extended_api.h"
#include "utils/logger_helper.h"
#include "utils/time.h"

class Tangle : public QResDIOTObject {
public:
  // Methods
  Tangle();
  ~Tangle();
  Tangle(std::string, int, bool, int, int, std::string);
  // Members
  // Signals
  sigslot::signal1<bundle_transactions_t *> BundleFetched;
  sigslot::signal1<std::string> BundleSent;
  // Slots
  virtual void AcceptRunModeOrder(int);
  void AcceptSendRequest(bundle_transactions_t *);
  void AcceptFetchRequest(std::string);
  void AcceptFetchRequest(std::vector<std::string>);
  void AcceptPriorityFetchRequest(std::string);
  void AcceptPriorityFetchRequest(std::vector<std::string>);
  std::vector<std::string> Do(std::string);

private:
  // Methods
  virtual void init();
  virtual void pause();
  virtual void run();
  virtual const std::string identify();
  void node_info();
  bool checkNode();
  bool sendBundle(bundle_transactions_t *);
  bundle_transactions_t *retrieveBundle(std::string);
  std::vector<std::string> getBundlesFromAdress(std::string);
  std::thread *startRetrieving();
  std::thread *startSending();
  // Members
  std::string m_host;
  int m_port;
  bool m_rempow;
  int m_mwm;
  int m_depth;
  std::string m_cacert;
  iota_client_service_t m_client_service;
  std::queue<bundle_transactions_t *> m_bundle_send_queue;
  std::queue<std::string> m_bundle_fetch_queue;
  std::queue<std::string> m_bundle_priority_fetch_queue;
  std::thread *m_receiver_thread;
  std::thread *m_sender_thread;
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

#endif // TANGLE_H
